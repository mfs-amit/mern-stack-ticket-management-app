import { useEffect, useState } from 'react';

export const useAuth = () => {
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        const checkAuth = async () => {
            const token: any = localStorage.getItem('token');
            if (token) {
                setIsAuth(token);
            }
        };
        checkAuth();
    }, []);

    return isAuth;
};
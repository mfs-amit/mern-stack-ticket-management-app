export const validator = (values: any, fieldName: string) => {
  const errors = {};
  switch (fieldName) {
    case "emailId":
      validateEmail(values.emailId, errors);
      break;
    case "password":
      validatePassword(values.password, errors);
      break;
    case "name":
      validateText(values.name, "Name", errors);
      break;
    case "title":
      validateText(values.title, "Title", errors);
      break;
    case "description":
      validateText(values.description, "Description", errors);
      break;
      case "commentText":
      validateText(values.commentText, "Comment", errors);
      break;
    case "productType":
      return true;
    default:
  }
  return errors;
};

function validateEmail(email: string, errors: any) {
  let result = true;

  if (!email) {
    errors.email = "Email is Required";
    result = false;
  } else {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    result = re.test(String(email).toLowerCase());
    if (!result) errors.email = "Invalid Email address";
  }
  return result;
}

function validatePassword(pass: string, errors: any) {
  let result = true;

  if (!pass) {
    errors.password = "Password is Required";
    result = false;
  } else {
    const lower = /(?=.*[a-z])/;
    result = lower.test(pass);

    if (!result) {
      errors.password = "Password must contain at least one lower case letter.";
      result = false;
    } else if (pass.length < 4) {
      errors.password = "Your password has less than 4 characters.";
      result = false;
    }
  }

  return result;
}

function validateText(text: string, field: string, errors: any) {
  let result = true;

  if (!text) {
    errors.password = `${field} is Required`;
    result = false;
  }

  return result;
}

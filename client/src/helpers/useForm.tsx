import { useState } from "react";
import { useMutation } from 'react-query';
interface UseForm {
    initState: any, callback: any, validator: any
}

const postData = async (values: any) => {
    const response = await fetch(`https://tma-server-63wv.onrender.com/${values.apiEndPoint}`, {
        method: values.method ? values.method : 'POST',
        headers: new Headers({
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        }),
        body: JSON.stringify(values)
    });
    return values.method ? response : response.json();
};

const useForm = (useFormState: UseForm) => {
    const [state, setState] = useState(useFormState?.initState);
    const [errors, setErrors] = useState<any>({});
    const { mutateAsync, isLoading, isError } = useMutation(postData, {
        onSuccess: (response) => {
            setState(() => ({
                ...state,
                isLoaded: true,
                isLoading,
                isError
            }));
            useFormState?.callback(response)
        },
        onError: (error) => {
            setState(() => ({
                ...state,
                isLoading,
                isError,
                error
            }));
        }
    });

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setState(() => ({
            ...state,
            [name]: value
        }));
    };

    const handleBlur = (e: any) => {
        const { name: fieldName } = e.target;
        const failedFields = useFormState?.validator(state, fieldName);
        setErrors(() => ({
            ...errors,
            [fieldName]: Object.values(failedFields)[0]
        }));
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        const { name: fieldName } = e.target;
        const failedFields = useFormState?.validator(state, fieldName);
        setErrors(() => ({
            ...errors,
            [fieldName]: Object.values(failedFields)[0]
        }));
        setState(() => ({
            ...state,
            isLoading: true,
            isError
        }));
        mutateAsync(state);
    };

    return {
        handleChange,
        handleSubmit,
        handleBlur,
        state,
        errors,
        setState
    };
};

export default useForm;

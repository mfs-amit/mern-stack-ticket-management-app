import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import './App.css';
import { createBrowserRouter, createRoutesFromElements, Outlet, Route, RouterProvider, } from "react-router-dom";
import CreateTicket from './components/Create-ticket/Create-ticket';
import ErrorPage from './components/Error/Error';
import Dashboard from './components/Dashboard/Dashboard';
import Login from './components/Login/Login';
import Landing from './components/Landing/Landing';
import Navbar from './components/Navbar/Navbar';

const queryClient = new QueryClient();

const AppLayout = () => (
  <>
    <Navbar />
    <Outlet />
  </>
);

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<AppLayout />}>
      <Route path="/" element={<Landing />} errorElement={<ErrorPage />} />
      <Route path="/create" element={<CreateTicket />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/login" element={<Login />} />
    </Route>
  ));

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">

        <RouterProvider router={router} />
      </div>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default App;

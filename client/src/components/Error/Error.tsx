import React from 'react';
import { Container } from "@mui/material";
import { useRouteError } from "react-router-dom";

const ErrorPage = () => {
  const error: any = useRouteError();

  return (
    <Container maxWidth="xl" sx={{ mt: 15 }}>
      <h1>Oops!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      <p>
        <i>{error?.statusText || error?.message}</i>
      </p>
    </Container>
  );
};

export default ErrorPage;

import React from 'react';
import {
    TextField,
    Button,
    Alert,
    Snackbar
} from "@mui/material";
import useForm from "../../helpers/useForm";
import { validator } from "../../helpers/validator";
import SendIcon from '@mui/icons-material/Send';

const AddComment = (data: any) => {
    const initState = {
        apiEndPoint: `tickets/${data.commentTicketId}/comments`,
        comment: "",
        ticketId: data.commentTicketId,
        isLoading: false,
        isError: false,
        error: null
    };

    const submit = (response: any) => {
        if (response && response.data && response.data.comments) {
            setTimeout(() => {
                setState(() => ({
                    ...state,
                    comment: "",
                    isLoaded: false
                }));
            }, 1000);
            data.childToParent();
        }
    };

    const { handleChange, handleSubmit, handleBlur, state, errors, setState } = useForm({
        initState,
        callback: submit,
        validator,
    });

    const isValidForm =
        Object.values(errors).filter((error) => typeof error !== "undefined")
            .length === 0;
    return (
        <form onSubmit={handleSubmit}>
            <Snackbar open={state.isLoaded} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    Comment added successfully!
                </Alert>
            </Snackbar>
            <div>
                <TextField
                    size="small"
                    margin="normal"
                    fullWidth
                    required
                    name="comment"
                    multiline
                    rows={4}
                    placeholder='Add comment'
                    value={state.comment}
                    onChange={handleChange}
                    error={!!errors.comment}
                    helperText={errors.comment}
                    onBlur={handleBlur}
                />
            </div>
            <div>
                <Button variant="contained" disabled={!isValidForm || state.isLoading}
                    type="submit" color="primary" endIcon={<SendIcon />}>
                    Add
                </Button>
            </div>
        </form>
    );
};
export default AddComment;

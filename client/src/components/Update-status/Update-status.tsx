import React from 'react';
import {
    TextField,
    Button,
    Alert,
    Snackbar,
    MenuItem
} from "@mui/material";
import useForm from "../../helpers/useForm";
import { validator } from "../../helpers/validator";


const statusType = [
    {
        value: "UNDER_REVIEW",
        label: 'UNDER_REVIEW'
    },
    {
        value: "FIXED",
        label: 'FIXED'
    }
]
const UpdateStatus = (data: any) => {
    const initState = {
        method: "PUT",
        apiEndPoint: `tickets/${data.commentTicketId}`,
        status: data.status,
        ticketId: data.commentTicketId,
        isLoaded: false,
        isLoading: false,
        isError: false,
        error: null
    };

    const submit = () => {
        setTimeout(() => {
            setState(() => ({
                ...state,
                comment: "",
                isLoaded: false
            }));
        }, 1000);
        data.childToParent();
    };

    const { handleChange, handleSubmit, handleBlur, state, errors, setState } = useForm({
        initState,
        callback: submit,
        validator,
    });

    const isValidForm =
        Object.values(errors).filter((error) => typeof error !== "undefined")
            .length === 0;
    return (
        <div>
            <Snackbar open={state.isLoaded} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    Ticket status updated successfully!
                </Alert>
            </Snackbar>

            <form onSubmit={handleSubmit} style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ minWidth: "200px" }}>
                    <TextField
                        size="small"
                        margin="normal"
                        fullWidth
                        required
                        select
                        name="status"
                        value={state.status}
                        onChange={handleChange}
                        error={!!errors.status}
                        onBlur={handleBlur}
                    >
                        {statusType.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                <div style={{ marginLeft: '10px', marginTop: "8px" }}>
                    <Button variant="contained" disabled={!isValidForm || state.isLoading}
                        type="submit" color="primary" >
                        Save
                    </Button>
                </div>
            </form>
        </div>
    );
};
export default UpdateStatus;

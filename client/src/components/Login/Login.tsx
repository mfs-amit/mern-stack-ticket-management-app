import React, { useEffect } from 'react';
import {
  TextField,
  Button,
  Card,
  CardHeader,
  CardContent,
} from "@mui/material";
import useForm from "../../helpers/useForm";
import { validator } from "../../helpers/validator";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate("/dashboard");
    }
  }, [navigate]);
  const initState = {
    apiEndPoint: "login",
    emailId: "",
    password: "",
    isLoading: false,
    isError: false,
    error: null
  };

  const submit = (response: any) => {
    if (response && response.data) {
      localStorage.setItem('token', response.data.token);
      navigate("/dashboard");
    }
  };

  const { handleChange, handleSubmit, handleBlur, state, errors } = useForm({
    initState,
    callback: submit,
    validator,
  });

  const isValidForm =
    Object.values(errors).filter((error) => typeof error !== "undefined")
      .length === 0;
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Card
        variant="outlined"
        sx={{
          maxWidth: 375,
          minWidth: 375,
          margin: 5,
          paddingLeft: 2,
          paddingRight: 2,
        }}
      >
        <CardHeader title="Welcome to TMA dashboard" />
        <CardContent>
          <form onSubmit={handleSubmit}>
            <div>
              <TextField
                margin="normal"
                fullWidth
                required
                label="Email"
                name="emailId"
                value={state.emailId}
                onChange={handleChange}
                error={!!errors.emailId}
                helperText={errors.emailId}
                onBlur={handleBlur}
              />
              <br />
              <TextField
                margin="normal"
                fullWidth
                required
                label="Password"
                name="password"
                type="password"
                value={state.password}
                onChange={handleChange}
                error={!!errors.password}
                helperText={errors.password}
                onBlur={handleBlur}
              />
            </div>
            <div>
              <Button
                sx={{ mt: 2, width: "100%" }}
                disabled={!isValidForm || state.isLoading}
                type="submit"
                variant="contained"
                color="primary"
              >
                {state.isLoading ? 'Please wait...' : 'Login'}
              </Button>
            </div>
          </form>
        </CardContent>
      </Card>
    </div>
  );
};
export default Login;

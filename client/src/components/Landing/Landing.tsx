import React from "react";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import ConfirmationNumberIcon from "@mui/icons-material/ConfirmationNumber";
import { Link } from "react-router-dom";

const Landing = () => {
  return (
    <Container maxWidth="xl" sx={{ mt: 15 }}>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "gray",
        }}
      >
        <ConfirmationNumberIcon
          sx={{ display: "flex", mr: 1, fontSize: "6rem" }}
        />
        <Typography
          variant="h1"
          noWrap
          component="a"
          href="/"
          sx={{
            mr: 2,
            display: "flex",
            fontFamily: "monospace",
            fontWeight: 700,
            letterSpacing: ".3rem",
            color: "inherit",
            textDecoration: "none",
          }}
        >
          TMA
        </Typography>
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "gray",
        }}
      >
        <Typography
          variant="h6"
          component="a"
          href="/"
          sx={{
            display: "flex",
            fontFamily: "monospace",
            fontWeight: 700,
            letterSpacing: ".3rem",
            color: "inherit",
            textDecoration: "none",
          }}
        >
          Ticket Management Application
        </Typography>
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "gray",
          marginTop: "1rem",
        }}
      >
        <Button component={Link} to="/dashboard" variant="contained">
          Manage Ticket
        </Button>
        <Button component={Link} to="/create" variant="outlined" sx={{ ml: 2 }}>
          Create Ticket+
        </Button>
      </div>
    </Container>
  );
};
export default Landing;

import React from "react";
import {
  TextField,
  Button,
  Card,
  CardHeader,
  CardContent,
  MenuItem,
  Alert,
  Snackbar,
} from "@mui/material";
import useForm from "../../helpers/useForm";
import { validator } from "../../helpers/validator";

const productType = [
  {
    value: "MOBILE_APP",
    label: "Mobile App",
  },
  {
    value: "WEBSITE",
    label: "Website",
  },
  {
    value: "SUBSCRIPTIONS",
    label: "Subscriptions",
  },
  {
    value: "GENERAL",
    label: "General",
  },
  {
    value: "OTHER",
    label: "Others",
  },
];

const CreateTicket = () => {
  const initState = {
    apiEndPoint: "tickets",
    emailId: "",
    name: "",
    title: "",
    description: "",
    productType: 'MOBILE_APP',
    isLoading: false,
    isLoaded: false,
    isError: false,
    error: null
  };

  const submit = (response: any) => {
    if (response && response.data) {
      setTimeout(() => {
        setState(() => ({
          ...state,
          emailId: "",
          name: "",
          title: "",
          description: "",
          productType: 'MOBILE_APP',
          isLoaded: false
        }));
      }, 1000);
    }
  };

  const { handleChange, handleSubmit, handleBlur, state, errors, setState } = useForm({
    initState,
    callback: submit,
    validator,
  });

  const isValidForm =
    Object.values(errors).filter((error) => typeof error !== "undefined")
      .length === 0;

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Snackbar open={state.isLoaded} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
        <Alert severity="success" sx={{ width: '100%' }}>
          Ticket created successfully!
        </Alert>
      </Snackbar>

      <Card
        variant="outlined"
        sx={{
          maxWidth: 375,
          minWidth: 375,
          margin: 5,
          paddingLeft: 2,
          paddingRight: 2,
        }}
      >
        <CardHeader title="Create Ticket" />
        <CardContent>
          <form onSubmit={handleSubmit}>
            <div>
              <TextField
                size="small"
                margin="normal"
                fullWidth
                required
                label="Name"
                name="name"
                value={state.name}
                onChange={handleChange}
                error={!!errors.name}
                helperText={errors.name}
                onBlur={handleBlur}
              />
              <br />
              <TextField
                size="small"
                margin="normal"
                fullWidth
                required
                label="Email"
                name="emailId"
                value={state.emailId}
                onChange={handleChange}
                error={!!errors.emailId}
                helperText={errors.emailId && errors.emailId}
                onBlur={handleBlur}
              />
              <br />
              <TextField
                size="small"
                margin="normal"
                fullWidth
                required
                label="Title"
                name="title"
                value={state.title}
                onChange={handleChange}
                error={!!errors.title}
                helperText={errors.title}
                onBlur={handleBlur}
              />
              <br />
              <TextField
                size="small"
                margin="normal"
                fullWidth
                required
                label="Description"
                multiline
                rows={4}
                name="description"
                value={state.description}
                onChange={handleChange}
                error={!!errors.description}
                helperText={errors.description}
                onBlur={handleBlur}
              />
              <br />
              <TextField
                size="small"
                margin="normal"
                fullWidth
                required
                select
                label="Product Type"
                name="productType"
                value={state.productType}
                onChange={handleChange}
                error={!!errors.productType}
                helperText={errors.productType}
                onBlur={handleBlur}
              >
                {productType.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <div>
              <Button
                sx={{ mt: 2, width: "100%" }}
                disabled={!isValidForm || state.isLoading}
                type="submit"
                variant="contained"
                color="primary"
              >
                {state.isLoading ? 'Please wait...' : 'Create'}
              </Button>
            </div>
          </form>
        </CardContent>
      </Card>
    </div>
  );
};

export default CreateTicket;
import React from 'react';
import {
    TextField,
    Button,
    Alert,
    Snackbar,
    MenuItem
} from "@mui/material";
import useForm from "../../helpers/useForm";
import { validator } from "../../helpers/validator";

const statusType = [
    {
        value: "UNDER_REVIEW",
        label: 'UNDER_REVIEW'
    },
    {
        value: "FIXED",
        label: 'FIXED'
    }
]
const AssignTicket = (ticketDetail: any) => {
    const initState = {
        apiEndPoint: `tickets/${ticketDetail.commentTicketId}/assign`,
        emailId: ticketDetail.ticketAssignedTo,
        ticketId: ticketDetail.commentTicketId,
        isLoaded: false,
        isLoading: false,
        isError: false,
        error: null
    };

    const submit = () => {
        setTimeout(() => {
            setState(() => ({
                ...state,
                comment: "",
                isLoaded: false
            }));
        }, 1000);
        ticketDetail.childToParent();
    };

    const { handleChange, handleSubmit, handleBlur, state, errors, setState } = useForm({
        initState,
        callback: submit,
        validator,
    });

    const isValidForm =
        Object.values(errors).filter((error) => typeof error !== "undefined")
            .length === 0;
    return (
        <div>
            <Snackbar open={state.isLoaded} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    Ticket Assigned successfully!
                </Alert>
            </Snackbar>

            <form onSubmit={handleSubmit} style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ minWidth: "200px" }}>
                    <TextField
                        size="small"
                        margin="normal"
                        fullWidth
                        required
                        select
                        name="emailId"
                        value={state.emailId}
                        onChange={handleChange}
                        error={!!errors.emailId}
                        onBlur={handleBlur}
                    >
                        {ticketDetail.usersList && ticketDetail.usersList.map((user: any) => (
                            <MenuItem key={user.emailId} value={user.emailId}>
                                {user.emailId}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                <div style={{ marginLeft: '10px', marginTop: "8px" }}>
                    <Button variant="contained" disabled={!isValidForm || state.isLoading}
                        type="submit" color="primary" >
                        Save
                    </Button>
                </div>
            </form>
        </div>
    );
};
export default AssignTicket;

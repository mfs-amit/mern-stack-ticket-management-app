import { AppBar, Avatar, Card, CardContent, CardHeader, Container, Dialog, Divider, Grid, IconButton, List, ListItem, Slide, Toolbar, Typography } from "@mui/material";
import { red } from "@mui/material/colors";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Moment from "react-moment";
import CloseIcon from "@mui/icons-material/Close";
import { TransitionProps } from "@mui/material/transitions";
import AddComment from "../Add-comment/Add-comment";
import UpdateStatus from "../Update-status/Update-status";
import AssignTicket from "../Assign-ticket/Assign-ticket";

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement;
    },
    ref: React.Ref<unknown>
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const getData = async (endpoint: string) => {
    const response = await fetch(endpoint, {
        method: "GET",
        headers: new Headers({
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/x-www-form-urlencoded",
        }),
    });
    return response.json();
};

const Dashboard = () => {
    const navigate = useNavigate();
    const ticketsData: any[] = [];
    const usersData: any[] = [];
    const [state, setState] = useState({ tickets: ticketsData, users: usersData });
    const [open, setOpen] = React.useState(false);
    let defaultSelectedTicket: any;
    const [selectedTicket, setTicket] = React.useState(defaultSelectedTicket);
    const [ticketId, setTicketId] = useState('');
    const [ticketStatus, setTicketStatus] = useState('');
    const [ticketAssignee, setTicketAssignee] = useState('');

    const sendTicketId = (ticketId: string) => {
        setTicketId(ticketId);
    }

    const sendTicketStatus = (status: string) => {
        setTicketStatus(status);
    }

    const sendTicketAssignee = (emailId: string) => {
        setTicketAssignee(emailId);
    }

    const handleClickOpen = (data: any) => {
        setTicket(data);
        sendTicketId(data._id);
        sendTicketStatus(data.status);
        sendTicketAssignee(data?.assignedTo?.emailId);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const fetchTickets = async () => {
        const ticketData = await getData(
            "https://tma-server-63wv.onrender.com/tickets"
        );
        const userData = await getData(
            "https://tma-server-63wv.onrender.com/users"
        );
        return setState({ ...state, tickets: [...ticketData.data], users: [...userData.data] });
    };

    const { tickets, users } = state;

    useEffect(() => {
        const token = localStorage.getItem("token");
        if (!token) {
            navigate("/login");
        }
        fetchTickets();
    }, []);

    const childToParent = async () => {
        const userData = await getData(
            "https://tma-server-63wv.onrender.com/tickets"
        );
        setState({ ...state, tickets: [...userData.data] });
        const matchedData = userData.data.find((data: any) => data._id === selectedTicket._id)
        setTicket(matchedData);
    }

    return (
        <div>
            <Container maxWidth="xl" sx={{ padding: 2 }}>
                <Grid
                    container
                    spacing={{ xs: 2, md: 3 }}
                    columns={{ xs: 2, sm: 8, md: 12 }}
                >
                    {tickets.map((ticket, index) => (
                        <Grid item xs={2} sm={4} md={4} key={index}>
                            <Card
                                sx={{ textAlign: "left", height: "100%", cursor: "pointer" }}
                                onClick={() => handleClickOpen(ticket)}
                            >
                                <div
                                    style={{
                                        position: "relative",
                                        textAlign: "right",
                                        display: "flex",
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <span
                                        style={{
                                            background: "rgb(0 0 0 / 4%)",
                                            padding: "5px",
                                            fontWeight: "600",
                                            fontSize: "12px",
                                        }}
                                    >
                                        {ticket.productType}
                                    </span>
                                    {ticket.status === "UNDER_REVIEW" ? (
                                        <span
                                            style={{
                                                background: "#fbfb0078",
                                                padding: "5px",
                                                fontWeight: "600",
                                                fontSize: "12px",
                                            }}
                                        >
                                            {ticket.status}
                                        </span>
                                    ) : (
                                        <span
                                            style={{
                                                background: "#adff2f8f",
                                                padding: "5px",
                                                fontWeight: "600",
                                                fontSize: "12px",
                                            }}
                                        >
                                            {ticket.status}
                                        </span>
                                    )}
                                </div>
                                <CardContent sx={{ height: "calc(100% - 160px)" }}>
                                    <Typography variant="h5">{ticket.title}</Typography>
                                    <Typography variant="caption" display="block">
                                        <Moment format="DD-MM-YYYY">{ticket.createdAt}</Moment>
                                    </Typography>
                                </CardContent>
                                <Typography
                                    variant="subtitle2"
                                    sx={{
                                        fontWeight: "600",
                                        color: "gray",
                                        marginLeft: "16px",
                                        marginTop: "16px",
                                    }}
                                >
                                    Created by
                                </Typography>
                                <CardHeader
                                    sx={{ padding: "10px 16px" }}
                                    avatar={
                                        <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                                            {ticket.submittedBy.name.charAt(0).toUpperCase()}
                                        </Avatar>
                                    }
                                    title={ticket.submittedBy.name}
                                    subheader={ticket.submittedBy.emailId}
                                />
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: "sticky" }}>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            Ticket detail
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Container maxWidth="xl" sx={{ padding: 2 }}>
                    <Grid
                        container
                        rowSpacing={1}
                        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                    >
                        <Grid item xs={12} sm={12} md={7}>
                            <Typography variant="h5">{selectedTicket?.title}</Typography>
                            <Typography variant="caption" display="block">
                                <Moment format="DD-MM-YYYY">{selectedTicket?.createdAt}</Moment>
                            </Typography>
                            <Typography sx={{ mt: 2, fontWeight: 600 }} variant="body1">Status</Typography>
                            <UpdateStatus childToParent={childToParent} status={ticketStatus} commentTicketId={ticketId} />
                            <Typography sx={{ mt: 2, fontWeight: 600 }} variant="body1">Assignee</Typography>
                            <AssignTicket childToParent={childToParent} ticketAssignedTo={ticketAssignee} usersList={users} commentTicketId={ticketId} />
                            <Typography sx={{ mt: 2, mb: 2, fontWeight: 600 }} variant="body1">Description</Typography>
                            <Typography variant="body2" gutterBottom>
                                {selectedTicket?.description}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={12} md={5}>
                            <Typography variant="h5" sx={{ mb: 2 }}>
                                Comment
                            </Typography>
                            <AddComment childToParent={childToParent} commentTicketId={ticketId} />
                            <List sx={{ width: '100%' }}>
                                {selectedTicket?.comments && selectedTicket.comments.map((comment: any, index: number) => (
                                    <div key={index}>
                                        <ListItem disableGutters>
                                            <div>
                                                <div>
                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component="span"
                                                        variant="h6"
                                                        color="text.primary"
                                                    >
                                                        {comment.author.name}
                                                    </Typography>
                                                </div>
                                                <div>
                                                    <Typography variant="caption" display="block">
                                                        <Moment format="DD-MM-YYYY">{comment.createdAt}</Moment>
                                                    </Typography>
                                                </div>
                                                <div>
                                                    <Typography
                                                        sx={{ display: 'inline', overflowWrap: 'anywhere' }}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary"
                                                    >
                                                        {comment.content}
                                                    </Typography>
                                                </div>
                                            </div>
                                        </ListItem>
                                        <Divider component="li" />
                                    </div>
                                ))}
                            </List>
                        </Grid>
                    </Grid>
                </Container>
            </Dialog>
        </div>
    );
};
export default Dashboard;

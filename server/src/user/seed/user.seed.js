const { encryptPassword } = require('../../auth/auth.util');
const dbConfig = require('../../config/dbConfig');
const User = require('../user.model');
const userRoles = require('../user.roles');
const fs = require('fs/promises');
const path = require('path');
const users = [
  {
    id:"1",
    name:"amit1",
    emailId:"amit1@yopmail.com",
    password:"amit1"
  },
  {
    id:"2",
    name:"amit2",
    emailId:"amit2@yopmail.com",
    password:"amit2"
  },
  {
    id:"3",
    name:"amit3",
    emailId:"amit3@yopmail.com",
    password:"amit3"
  },
  {
    id:"4",
    name:"amit4",
    emailId:"amit4@yopmail.com",
    password:"amit4"
  },
  {
    id:"5",
    name:"amit5",
    emailId:"amit5@yopmail.com",
    password:"amit5"
  }
];
(async function () {
  let mongoose;
  try {
    const finalUsers = await Promise.all(
      users.map(({ id, ...user }) => {
        return new Promise((resolve, reject) => {
          encryptPassword(user.password).then((encryptedPassword) => {
            resolve({
              ...user,
              password: encryptedPassword,
              role: Object.values(userRoles)[Math.floor(Math.random() * 2)],
            });
          });
        });
      })
    );
    mongoose = await dbConfig();

    await User.insertMany(finalUsers);
    await fs.writeFile(
      path.resolve(process.cwd(), 'users.txt'),
      JSON.stringify(
        users.map((user, index) => ({
          ...user,
          role: finalUsers[index].role,
        })),
        null,
        4
      )
    );
  } catch (err) {
    console.log({ err });
  } finally {
    await mongoose.disconnect();
  }
})();

const CONSTANTS = {
  EMAIL_ID_IS_REQUIRED: 'Email is required.',
  PASSWORD_IS_REQUIRED: 'Password is required.',
  NAME_IS_REQUIRED: 'name is required.',
  EMAIL_ID_REGEXP: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/,
  TICKET_RELATED_PRODUCT_REQUIRED: 'Product related to ticket is required.',
  COMMENT_CANNOT_BE_EMPTY: 'Comment is required',
};

module.exports = CONSTANTS;

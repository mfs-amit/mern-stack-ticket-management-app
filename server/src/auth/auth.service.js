const ForbiddenException = require('../shared/exception/ForbiddenException');
const NotFoundException = require('../shared/exception/NotFoundException');
const User = require('../user/user.model');
const { comparePassword, createJwtToken } = require('./auth.util');

const loginUser = async ({ emailId, password }) => {
  const user = await User.findOne({
    emailId,
  });
  if (!user) {
    throw new NotFoundException("The Email or Password doesn't match. Please try again!");
  }
  const isPasswordSame = await comparePassword(password, user.password);
  if (!isPasswordSame) {
    throw new ForbiddenException("The Email or Password doesn't match. Please try again!");
  }
  const token = await createJwtToken({
    _id: user.id,
    role: user.role,
    emailId: user.emailId,
  });
  return {
    emailId: user.emailId,
    name: user.name,
    role: user.role,
    token,
  };
};

module.exports = {
  loginUser,
};
